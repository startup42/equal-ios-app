//
//  CoreDataUtils.swift
//  equal
//
//  Created by Alban on 06/03/2021.
//

import UIKit
import CoreData

struct CoreDataUtils {
    private static var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private static let defaultErrorHandler: () -> Void = {
        print("Error in CoreData")
        
    }
    public static func fetchResponse<T: NSManagedObject>(objectType: T.Type,
                                                         predicate: NSPredicate? = nil,
                                                         errorHandler: (() -> Void) = defaultErrorHandler) -> [T]? {
        do {
            let request = NSFetchRequest<T>(entityName: T.className)
            request.predicate = predicate
            return try context.fetch(request)
        } catch {
            errorHandler()
        }
        return nil
    }

    public static func delete(response: NSManagedObject, errorHandler: (() -> Void) = defaultErrorHandler) {
        context.delete(response)
        do {
            try context.save()
            
        } catch {
            errorHandler()
        }
        
    }
    public static func clearTable<T: NSManagedObject>(objectType: T.Type, errorHandler: (() -> Void) = defaultErrorHandler) {
        let request: NSFetchRequest<NSFetchRequestResult> = T.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        do {
            try context.execute(deleteRequest)
        } catch {
            errorHandler()
        }
    }
}

// MARK: - CDFavoriteKey
extension CoreDataUtils {
    /*
     public static func saveFavoriteKeys(key: String, date: String?, errorHandler: (() -> Void) = defaultErrorHandler) {
                     let response = CDFavoriteKey(context: self.context)
                     response.key = key
                     response.date = date
                     response.deletedKey = false
                     do {
                             try self.context.save()
                     } catch {
                             errorHandler()
                     }
             }*/
}
