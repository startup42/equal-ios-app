//
//  BaseRequest.swift
//  equal
//
//  Created by Alban on 27/02/2021.
//

import Foundation
import Alamofire

typealias Completion<T: Decodable> = (Result<T, Error>) -> Void

class BaseRequest {
    
    private var baseUrl: String {
        guard let url = Bundle.main.infoDictionary?["BASE_URL"] as? String else { return "" }
        return url
    }
    
    func GET<T: Decodable>(_ url: String, completion: @escaping Completion<T>) {
        AF.request(baseUrl + url,
                   method: .get)
            .responseJSON { response in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do {
                        let decodable = try JSONDecoder().decode(T.self, from: jsonData!)
                        completion(.success(decodable))
                    } catch let error {
                        completion(.failure(error))
                    }
                case let .failure(error):
                    print("Error while fetching data \(String(describing: error))")
                }
            }
    }
}
